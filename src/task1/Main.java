package task1;

/**
 * Задание: Спроектировать объектную модель для заданной предметной области.
 *
 * Vladimir Plekhanov
 */
public class Main {
    public static void main(String[] args) {
        ActionWithTransportData action1 = new ActionWithTransportData();
        action1.getListTransport();

        TotalCostOfTransport action2 = new TotalCostOfTransport();
        action2.calculateCostOfTransport();
    }
}