package task1;

public class Transport {
    private final Type type;
    private final int seats;
    private final Engine engine;
    private final Transmission transmission;
    private final int totalSeats;
    private final double brakingDistances;
    private final int price;

    public Transport(Type type, int seats, Engine engine, Transmission transmission,
                     int totalSeats, double brakingDistances, int price) {
        this.type = type;
        this.seats = seats;
        this.engine = engine;
        this.transmission = transmission;
        this.totalSeats = totalSeats;
        this.brakingDistances = brakingDistances;
        this.price = price;
    }

    public Type getType() {
        return type;
    }

    public int getSeats() {
        return seats;
    }

    public Engine getEngine() {
        return engine;
    }

    public Transmission getTransmission() {
        return transmission;
    }

    public int getTotalSeats() {
        return totalSeats;
    }

    public double getBrakingDistances() {
        return brakingDistances;
    }

    public int getPrice() {
        return price;
    }
}
