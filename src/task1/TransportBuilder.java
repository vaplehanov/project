package task1;

public class TransportBuilder implements Builder {
    private Type type;
    private int seats;
    private Engine engine;
    private Transmission transmission;
    private int totalSeats;
    private double brakingDistances;
    private int price;

    @Override
    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public void setSeats(int seats) {
        this.seats = seats;
    }

    @Override
    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    @Override
    public void setTransmission(Transmission transmission) {
        this.transmission = transmission;
    }

    @Override
    public void setTotalSeats(int totalSeats) {
        this.totalSeats = totalSeats;
    }

    @Override
    public void setBrakingDistances(double brakingDistances) {
        this.brakingDistances = brakingDistances;
    }

    @Override
    public void setPrice(int price) {
        this.price = price;
    }

    public Transport getResult() {
        return new Transport(type, seats, engine, transmission, totalSeats, brakingDistances, price);
    }
}

