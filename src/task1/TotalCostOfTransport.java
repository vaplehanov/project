package task1;

public class TotalCostOfTransport {
    public void calculateCostOfTransport(){
        Director director = new Director();

        TransportBuilder builder1 = new TransportBuilder();
        director.constructPAZ(builder1);
        Transport transport1 = builder1.getResult();

        TransportBuilder builder2 = new TransportBuilder();
        director.constructGAZ(builder2);
        Transport transport2 = builder2.getResult();

        TransportBuilder builder3 = new TransportBuilder();
        director.constructMAZ(builder3);
        Transport transport3 = builder3.getResult();

        TransportBuilder builder4 = new TransportBuilder();
        director.constructSCANIA(builder4);
        Transport transport4 = builder4.getResult();

        int sum = transport1.getPrice() + transport2.getPrice() + transport3.getPrice() + transport4.getPrice();

        System.out.println("Total price(RUB): " + sum);
    }
}
