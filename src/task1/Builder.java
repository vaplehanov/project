package task1;

public interface Builder {
    void setType(Type type);
    void setSeats(int seats);
    void setEngine(Engine engine);
    void setTransmission(Transmission transmission);
    void setTotalSeats(int totalSeats);
    void setBrakingDistances(double brakingDistances);
    void setPrice(int price);
}
