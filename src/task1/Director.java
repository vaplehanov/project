package task1;

public class Director {

    public void constructPAZ(Builder builder) {
        builder.setType(Type.PAZ);
        builder.setSeats(16);
        builder.setEngine(new Engine(4.25, 23));
        builder.setTransmission(Transmission.MANUAL);
        builder.setTotalSeats(28);
        builder.setBrakingDistances(32.1);
        builder.setPrice(1700000);
    }

    public void constructGAZ(Builder builder) {
        builder.setType(Type.GAZ);
        builder.setSeats(12);
        builder.setEngine(new Engine(2.89, 11.5));
        builder.setTransmission(Transmission.MANUAL);
        builder.setTotalSeats(12);
        builder.setBrakingDistances(32.1);
        builder.setPrice(1100000);
    }

    public void constructMAZ(Builder builder) {
        builder.setType(Type.MAZ);
        builder.setSeats(25);
        builder.setEngine(new Engine(4.8, 22));
        builder.setTransmission(Transmission.AUTOMATIC);
        builder.setTotalSeats(72);
        builder.setBrakingDistances(32.1);
        builder.setPrice(6233000);
    }

    public void constructSCANIA(Builder builder) {
        builder.setType(Type.SCANIA);
        builder.setSeats(39);
        builder.setEngine(new Engine(9, 28.5));
        builder.setTransmission(Transmission.AUTOMATIC);
        builder.setTotalSeats(121);
        builder.setBrakingDistances(34.5);
        builder.setPrice(8905000);
    }
}
