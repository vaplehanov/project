package task1;

public class Engine {
    private final double volume;
    private final double fuelConsumption;

    public Engine(double volume, double fuelConsumption) {
        this.volume = volume;
        this.fuelConsumption = fuelConsumption;
    }

    public double getVolume() {
        return volume;
    }

    public double getFuelConsumption() {
        return fuelConsumption;
    }

}
