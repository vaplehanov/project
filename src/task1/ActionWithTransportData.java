package task1;

public class ActionWithTransportData {
    public void getListTransport(){
        Director director = new Director();

        System.out.println("Transport built:");

        TransportBuilder builder1 = new TransportBuilder();
        director.constructPAZ(builder1);
        Transport transport1 = builder1.getResult();
        System.out.println(transport1.getType());

        TransportBuilder builder2 = new TransportBuilder();
        director.constructGAZ(builder2);
        Transport transport2 = builder2.getResult();
        System.out.println(transport2.getType());

        TransportBuilder builder3 = new TransportBuilder();
        director.constructMAZ(builder3);
        Transport transport3 = builder3.getResult();
        System.out.println(transport3.getType());

        TransportBuilder builder4 = new TransportBuilder();
        director.constructSCANIA(builder4);
        Transport transport4 = builder4.getResult();
        System.out.println(transport4.getType());
    }
}

