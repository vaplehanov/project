package task1;

public class Manual {
    private final Type type;
    private final int seats;
    private final Engine engine;
    private final Transmission transmission;
    private final int totalSeats;
    private final double brakingDistances;
    private final int price;

    public Manual(Type type, int seats, Engine engine, Transmission transmission, int totalSeats, double brakingDistances, int price) {
        this.type = type;
        this.seats = seats;
        this.engine = engine;
        this.transmission = transmission;
        this.totalSeats = totalSeats;
        this.brakingDistances = brakingDistances;
        this.price = price;
    }

    public String print() {
        String info = "";
        info += "Type of car: " + type + "\n";
        info += "Count of seats: " + seats + "\n";
        info += "Engine: volume - " + engine.getVolume() + "; fuelConsumption - " + engine.getFuelConsumption() + "\n";
        info += "Transmission: " + transmission + "\n";
        info += "Total number of seats: " + totalSeats + "\n";
        info += "Braking distance from 60-80 km/h: meters - " + brakingDistances + "\n";
        info += "Price: " + price + "\n";
        return info;
    }
}
