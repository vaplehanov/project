package task2;

public class FilterListRunner {
    public static void main(String[] args) {
        ListGenerator listGenerator = new ListGenerator();

        int[] fL01 = listGenerator.generateList(10);
        int[] pL01 = listGenerator.generatePredicate(3);

        FilterList fList01 = new FilterList(fL01, pL01);

        System.out.println("List and predicate");
        listGenerator.listPrinter(fList01.getFilterList());
        listGenerator.listPrinter(fList01.getPredicate());
        System.out.println("Iterator result");
        listGenerator.listPrinter(fList01.iterate(fList01));

    }
}
